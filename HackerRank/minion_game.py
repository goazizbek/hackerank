def minion_game(s):
    l = len(s)
    stuart = 0
    kevin = 0

    for i in range(l):
        if s[i] in ['A', 'E', 'O', 'U', 'I']:
            kevin += l - i
        else:
            stuart += l - i

    if kevin > stuart:
        print('Kevin', kevin)
    elif stuart > kevin:
        print('Stuart', stuart)
    else:
        print('Draw')


if __name__ == '__main__':
    s = input()
    minion_game(s)