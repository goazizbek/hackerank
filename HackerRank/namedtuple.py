from collections import OrderedDict

N = int(input())

d = OrderedDict()

for i in range(N):
    item, price = [x for x in input().split()]

    d[item] = d.get(item, 0) + int(price)

# print([' '.join([item, str(price )]) for item, price in d.items()], sep='\n')

for i, j in d.items():
    print(i, j)