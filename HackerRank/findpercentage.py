if __name__ == 'main':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()

    sum = 0
    if query_name in student_marks:
      score = student_marks[query_name]
      st_no = len(scores)

      for i in scores:
        sum += i

    avg = sum / st_no
    print(avg)