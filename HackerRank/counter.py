from collections import Counter



X = int(input())
shoe_sizes = Counter(map(int, input().split()))
N = int(input())

cost = 0
for _ in range(N):
    size, price = map(int, input().split())
    if shoe_sizes[size]:
        cost += price
        shoe_sizes[size] -= 1

print(cost)
