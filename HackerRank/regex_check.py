import re

n = int(input())

for i in range(n):
    a = input()
    try:
        re.compile(a)
        print('True')
    except re.error:
        print('False')